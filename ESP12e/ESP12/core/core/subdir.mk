################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
..\ESP12\core\core\Crypto.cpp.o \
..\ESP12\core\core\Esp-frag.cpp.o \
..\ESP12\core\core\Esp-version.cpp.o \
..\ESP12\core\core\Esp.cpp.o \
..\ESP12\core\core\FS.cpp.o \
..\ESP12\core\core\FSnoop.cpp.o \
..\ESP12\core\core\FunctionalInterrupt.cpp.o \
..\ESP12\core\core\HardwareSerial.cpp.o \
..\ESP12\core\core\IPAddress.cpp.o \
..\ESP12\core\core\LwipDhcpServer-NonOS.cpp.o \
..\ESP12\core\core\LwipDhcpServer.cpp.o \
..\ESP12\core\core\LwipIntf.cpp.o \
..\ESP12\core\core\LwipIntfCB.cpp.o \
..\ESP12\core\core\MD5Builder.cpp.o \
..\ESP12\core\core\Print.cpp.o \
..\ESP12\core\core\Schedule.cpp.o \
..\ESP12\core\core\StackThunk.cpp.o \
..\ESP12\core\core\Stream.cpp.o \
..\ESP12\core\core\StreamSend.cpp.o \
..\ESP12\core\core\Tone.cpp.o \
..\ESP12\core\core\TypeConversion.cpp.o \
..\ESP12\core\core\Updater.cpp.o \
..\ESP12\core\core\WMath.cpp.o \
..\ESP12\core\core\WString.cpp.o \
..\ESP12\core\core\abi.cpp.o \
..\ESP12\core\core\aes_unwrap.cpp.o \
..\ESP12\core\core\base64.cpp.o \
..\ESP12\core\core\cbuf.cpp.o \
..\ESP12\core\core\cont.S.o \
..\ESP12\core\core\cont_util.cpp.o \
..\ESP12\core\core\core_esp8266_app_entry_noextra4k.cpp.o \
..\ESP12\core\core\core_esp8266_eboot_command.cpp.o \
..\ESP12\core\core\core_esp8266_features.cpp.o \
..\ESP12\core\core\core_esp8266_flash_quirks.cpp.o \
..\ESP12\core\core\core_esp8266_flash_utils.cpp.o \
..\ESP12\core\core\core_esp8266_i2s.cpp.o \
..\ESP12\core\core\core_esp8266_main.cpp.o \
..\ESP12\core\core\core_esp8266_non32xfer.cpp.o \
..\ESP12\core\core\core_esp8266_noniso.cpp.o \
..\ESP12\core\core\core_esp8266_phy.cpp.o \
..\ESP12\core\core\core_esp8266_postmortem.cpp.o \
..\ESP12\core\core\core_esp8266_si2c.cpp.o \
..\ESP12\core\core\core_esp8266_sigma_delta.cpp.o \
..\ESP12\core\core\core_esp8266_spi_utils.cpp.o \
..\ESP12\core\core\core_esp8266_timer.cpp.o \
..\ESP12\core\core\core_esp8266_vm.cpp.o \
..\ESP12\core\core\core_esp8266_waveform_phase.cpp.o \
..\ESP12\core\core\core_esp8266_waveform_pwm.cpp.o \
..\ESP12\core\core\core_esp8266_wiring.cpp.o \
..\ESP12\core\core\core_esp8266_wiring_analog.cpp.o \
..\ESP12\core\core\core_esp8266_wiring_digital.cpp.o \
..\ESP12\core\core\core_esp8266_wiring_pulse.cpp.o \
..\ESP12\core\core\core_esp8266_wiring_pwm.cpp.o \
..\ESP12\core\core\core_esp8266_wiring_shift.cpp.o \
..\ESP12\core\core\crc32.cpp.o \
..\ESP12\core\core\debug.cpp.o \
..\ESP12\core\core\exc-c-wrapper-handler.S.o \
..\ESP12\core\core\exc-sethandler.cpp.o \
..\ESP12\core\core\flash_hal.cpp.o \
..\ESP12\core\core\gdb_hooks.cpp.o \
..\ESP12\core\core\heap.cpp.o \
..\ESP12\core\core\hwdt_app_entry.cpp.o \
..\ESP12\core\core\libc_replacements.cpp.o \
..\ESP12\core\core\mmu_iram.cpp.o \
..\ESP12\core\core\reboot_uart_dwnld.cpp.o \
..\ESP12\core\core\spiffs_api.cpp.o \
..\ESP12\core\core\sqrt32.cpp.o \
..\ESP12\core\core\stdlib_noniso.cpp.o \
..\ESP12\core\core\time.cpp.o \
..\ESP12\core\core\uart.cpp.o 


# Each subdirectory must supply rules for building sources it contributes

