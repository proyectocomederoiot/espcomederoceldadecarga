#ifndef TCP_H
#define TCP_H

#include <ESP8266WiFi.h>
#include "Paquete.h"

int recibeTCP(WiFiClient client, Paquete &p, int timeOut);
bool enviaTCP(WiFiClient client, Paquete &p);
bool enviaTCP(WiFiClient client, String mensaje);

#endif
