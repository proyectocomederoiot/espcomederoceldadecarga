
#include "Tcp.h"

int recibeTCP(WiFiClient client, Paquete &p, int timeOut) {
	unsigned long time = millis();
	int i = 0;
	int ant = 0;
	bool stop = false;
	do {
		while (client.available() > 0) {
			char c = client.read();
			p.frame[i] = c;
			i++;
			ant = i;
			time = millis();
			if (i == MAX_DATA + 2) {
				stop = true;
				break;
			}
		}
		if (i > 1 && p.frame[1]==i-2)// se ha leido el paquete completo
			break;// sale del do-while. Paquete completo!!
		if (millis() - time > timeOut)
			break; // sale del do-while. Ocurre un Time-out en la entrega de paquetes
		if(!(client.available() > 0)) //Solo si no hay datos se espera
			delay(100);
	} while (!stop);
	bool error = false;
	while (client.available() > 0) { // Error: se envian muchos datos. Se consume lo que queda en el canal
		char c = client.read();
		error = true;
	}
	if (error)      // Error de formato en paquete recibido
		return -1;
	desEmpaqueta(p);
	if (p.l != i - 2) // Error de formato en paquete recibido
		return -1;
	return i;
}
bool enviaTCP(WiFiClient client, Paquete &p) { //para enviar bytes
	empaqueta(p);
	if (client.connected()) { // This will send a string to the server
		client.print((char)p.frame[0]); // envia codigo tipoPaquete
		client.print((char)p.frame[1]); // envia largo paquete
		for (int i = 0; i < p.l; i++)
			client.print((char)p.frame[i + 2]);
		return true;
	} else {
		return false;
	}
}
bool enviaTCP(WiFiClient client, String mensaje) { // para enviar texto
	if (client.connected()) { // This will send a string to the server
		client.print(mensaje);
		return true;
	} else {
		return false;
	}
}
