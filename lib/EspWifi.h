/*
 * EspWifi.h
 *
 *  Created on: 09-09-2021
 *      Author: ramiro
 */

#ifndef LIB_ESPWIFI_H_
#define LIB_ESPWIFI_H_

#include <Arduino.h>

bool conectarWifi(String ssid, String password, byte ip[], byte gateway[], byte subnet[], byte primaryDNS[]);
bool conectarWifi(String ssid, String password, byte ip[]);
bool conectarWifi(String ssid, String password);



#endif /* LIB_ESPWIFI_H_ */
