/*
 * EspWifi.cpp
 *
 *  Created on: 09-09-2021
 *      Author: ramiro
 */
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#include "EspWifi.h"

bool conectarWifi(String ssid, String password, byte ip[]){
	/*
	byte gateway[]={192, 168, 0, 1};
	byte subnet[]={255, 255, 255, 0};
	byte primaryDNS[]={192, 168, 0, 1};
	*/
	// IP 192.168.45.160
	byte gateway[4];	//    192.168.45.1
	byte subnet[4];		//	  255.255.255.0
	byte primaryDNS[4]; //	  192.168.45.1
	for(int j=0;j<4;j++){
		gateway[j] = 255;
		subnet[j] = ip[j];
		primaryDNS[j] = ip[j];
	}
	gateway[3]=1;
	subnet[3]=0;
	primaryDNS[3]=1;

	return conectarWifi(ssid, password, ip, gateway, subnet, primaryDNS);
}

bool conectarWifi(String ssid, String password, byte ip[], byte gateway[], byte subnet[], byte primaryDNS[]){
	WiFi.mode(WIFI_STA);
	while(!WiFi.enableSTA(true)) {
		Serial.print("-");
	}
	if (!WiFi.config(ip, gateway, subnet, primaryDNS)){
		Serial.println("Error al configurar ESP Static IP en modo STA.");
		Serial.print(((int)ip[0])+".");
		Serial.print(((int)ip[1])+".");
		Serial.print(((int)ip[2])+".");
		Serial.print(((int)ip[3])+".");
		return false;
	}
	Serial.print(((int)ip[0])+".");
	Serial.print(((int)ip[1])+".");
	Serial.print(((int)ip[2])+".");
	Serial.print(((int)ip[3])+".");
	Serial.print("Iniciando modo STA: ESP Static IP configurado. Conectando a: ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);
	int intento = 0;
	int intentoMax = 25;
	while (WiFi.status() != WL_CONNECTED && intento <= intentoMax) {
		delay(500);
		Serial.print(".");
		intento++;
	}
	if (intento > intentoMax)
		return false;
	WiFi.setAutoReconnect(true);
	return true;
}

bool conectarWifi(String ssid, String password){
	WiFi.mode(WIFI_STA);
	Serial.print("Iniciando modo STA-DHCP. Conectando a: ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);
	int intento = 0;
	int intentoMax = 25;
	while (WiFi.status() != WL_CONNECTED && intento <= intentoMax) {
		delay(500);
		Serial.print(".");
		intento++;
	}
	if (intento > intentoMax)
		return false;
	WiFi.setAutoReconnect(true);
	return true;
}


