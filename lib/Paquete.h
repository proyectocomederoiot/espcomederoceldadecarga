#ifndef PAQUETE_H
#define PAQUETE_H

#include <Arduino.h>

#include "Macros.h"

struct Paquete{
  int tipoPaquete; // tipo del paquete
  int l; //largo de la data en el paquete.
  unsigned char data[MAX_DATA]; // data máxima del paquete.
  unsigned char frame[MAX_DATA+2]; // cabecera + data.
  Paquete(){
    tipoPaquete = PAQ_NONE;
    l=0;
  }
};


void empaqueta(Paquete &p);
void desEmpaqueta(Paquete &p);

String toString(Paquete &p);

#endif
