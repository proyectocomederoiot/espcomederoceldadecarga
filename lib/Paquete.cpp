#include "Paquete.h"

String toString(Paquete &p) {
  String data = "";
  for (int i = 0; i < p.l; i++) 
    data = data + "byte(" + (char)p.data[i]+","+(int)p.data[i] + ") ";
  String mensaje = "";
  mensaje = mensaje + "tipo: |" + p.tipoPaquete + "| long: |" + p.l + "| data: |" + data + "|";
  return mensaje;
}

// | 1 BYTE     | 1 BYTE | L BYTES |
// |TIPO_PAQUETE|   L    |   DATA  |

void empaqueta(Paquete &p) {
  p.frame[0] = p.tipoPaquete & 0xFF;// Primer byte corresponde al tipo de paquete.
  p.l = strlen((char*)p.data);
  if(p.l > MAX_DATA)
    p.l=MAX_DATA;
  p.frame[1] = p.l & 0xFF;          // Segundo byte corresponde al largo de la data
  for (int i = 0; i < p.l; i++)
    p.frame[i + 2] = p.data[i];     // Desde el tercer byte comienza la DATA.
}

void desEmpaqueta(Paquete &p) {
  p.tipoPaquete = p.frame[0];
  p.l = p.frame[1];
  if(p.l > MAX_DATA)
    p.l=MAX_DATA;
  for (int i = 0; i < p.l; i++)
    p.data[i] = p.frame[i + 2];
}
