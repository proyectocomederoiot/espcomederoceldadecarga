#ifndef MACROS_H
#define MACROS_H

// Opciones de debug
#define LAN_DEBUG false

#define MAX_DATA 100 //Max 255

// Tipos de paquete
#define PAQ_NONE -1
#define PAQ_ID 2
#define PAQ_PRINT 3
#define PAQ_IP 1
#define PAQ_FUNC 5
#define PAQ_COMANDO 9

//define funcionalidades
#define NONE 0
#define FUNC_ULTRASONICO 21
#define FUNC_PESO 22
#define FUNC_SERVO 23

//define data
#define DATA_ULTRASONICO -21
#define DATA_PESO -22

#define CANT_PORCION 200
#endif
