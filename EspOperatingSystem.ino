#include "Arduino.h"

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#include "lib/EspWifi.h"
#include "lib/Config.h"
#include "lib/Tcp.h"
#include "lib/Utils.h"
#include "lib/Ota.h"
//#include "lib/NewPingESP8266.h"
#include "HX711.h"

const int dout = 14; //D5
const int sck  = 12; //D6
HX711 balanza;
//WiFiServer wifiServer(5000);
int i;
bool errorConn = false;


//NewPingESP8266 sonar(TRIGGER_PIN,ECHO_PIN,MAX_DISTANCE);

void setup() {

	InitOTA();
	Serial.begin(9600);
	Serial.println("Hola");
	Serial.println("SOY ESP12-e AQUI ESTOY!!!");


	Serial.begin(9600);
	balanza.begin(dout,sck);
	balanza.set_scale(775.68);
	balanza.tare(20);  // Hacer 10 lecturas, el promedio es la tara

	Serial.println("Listo para pesar");


	if(conectarWifi(ssid, password)){
		Serial.println("WiFi DCHP IP connected");
		Serial.print("IP address: -> ");
		Serial.println(WiFi.localIP().toString());
		Serial.print("MAC address: -> ");	Serial.println(WiFi.macAddress());
		Serial.print("Signal: ");		Serial.println(WiFi.RSSI());

		WiFiClient client;
		if (client.connect(serverHost, serverPort)){ // se conecta DCHP al servidor: IP dinámica
			Serial.println("Server Conected...."+serverHost+":"+serverPort);
			Paquete paqIp;
			paqIp.tipoPaquete = PAQ_IP;
			String mac = WiFi.macAddress() + "";
			String ip = WiFi.localIP().toString();
			String mensaje = mac+ip;
			mensaje.toCharArray((char*)paqIp.data, MAX_DATA);
			enviaTCP(client, paqIp); // se envía paquete de identificación al servidor

			//pinMode(4, OUTPUT);
		}else{
			Serial.println("Conexión fallida con el servidor "+serverHost+":"+serverPort);
			Serial.print("IP Local: -> ");
			Serial.println(WiFi.localIP().toString());
			errorConn=true;
		}
	}else{
		Serial.println("No se pudo conectar wifi en modo DHCP.");
		Serial.print("IP address: -> ");
		Serial.println(WiFi.localIP().toString());
		errorConn=true;
	}
	i =0;

	//python espota.py -i 192.168.1.160 -f ./Release/WifiMCU.bin -d -r
	//python esptool.py --port /dev/ttyUSB0 write_flash 0x00000 ./Release/WifiMCU.bin

}

void loop() {

	ArduinoOTA.handle();

	if(errorConn){
		Serial.println("Error de conexión....");
		Serial.print("IP address: -> ");
		Serial.println(WiFi.localIP().toString());
	}else{
		Serial.print("Signal rssi: ");
		Serial.println(WiFi.RSSI());
		WiFiClient client;
		if (!client.connect(serverHost, serverPort)){
			Serial.println("Conexión fallida con el servidor "+serverHost+":"+serverPort);
		}else{
			//Paquete p;
			//p.tipoPaquete = PAQ_PRINT;
			//String mensaje = "Hola Mundo from ESP8266 "+String(i);
			//i++;
			//mensaje.toCharArray((char*)p.data, MAX_DATA);
			//p.l = strlen((char*)p.data);
			//String aux = toString(p);
			//Serial.println(aux);
			//enviaTCP(client, p);
			Paquete p;
			//int dist= (int)(random(20,4000)); //Valor en milimetros.

			Serial.print("Valor de lectura: t");
			int peso = balanza.get_value(10);
			if (peso>0 && peso<16000){
						peso=0;
					}else{
						peso=70;
			}
			p.tipoPaquete =FUNC_PESO;
			p.data[0]=peso&0xFF;
			p.data[1]=(peso&0xFF00)>>8;
			p.l = 2;
			enviaTCP(client, p);
			delay(2000);
			//Queda pendiente Empaquetar y Enviar(ESP) , Recibir en administraci�n (convertir a valor real) y guardar en BD.
		}
	}
}
