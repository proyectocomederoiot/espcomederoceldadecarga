################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
..\lib\EspWifi.cpp \
..\lib\NewPingESP8266.cpp \
..\lib\Ota.cpp \
..\lib\Paquete.cpp \
..\lib\Tcp.cpp \
..\lib\Utils.cpp 

LINK_OBJ += \
.\lib\EspWifi.cpp.o \
.\lib\NewPingESP8266.cpp.o \
.\lib\Ota.cpp.o \
.\lib\Paquete.cpp.o \
.\lib\Tcp.cpp.o \
.\lib\Utils.cpp.o 

CPP_DEPS += \
.\lib\EspWifi.cpp.d \
.\lib\NewPingESP8266.cpp.d \
.\lib\Ota.cpp.d \
.\lib\Paquete.cpp.d \
.\lib\Tcp.cpp.d \
.\lib\Utils.cpp.d 


# Each subdirectory must supply rules for building sources it contributes
lib\EspWifi.cpp.o: ..\lib\EspWifi.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:\Eclipse\arduinoPlugin\packages\esp8266\tools\xtensa-lx106-elf-gcc\3.0.4-gcc10.3-1757bed/bin/xtensa-lx106-elf-g++" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ -D_GNU_SOURCE "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/lwip2/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/libc/xtensa-lx106-elf/include" "-IC:\Eclipse\workspace\espcomedero\ESP01S/core" -c -w -Werror=return-type  -Os -g -free -fipa-pta -mlongcalls -mtext-section-literals -fno-rtti -falign-functions=4 -std=gnu++17 -MMD -ffunction-sections -fdata-sections -fno-exceptions  -DMMU_IRAM_SIZE=0x8000 -DMMU_ICACHE_SIZE=0x8000  -DNONOSDK22x_190703=1 -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=536 -DLWIP_FEATURES=1 -DLWIP_IPV6=0   -DARDUINO=10812 -DARDUINO_ESP8266_GENERIC -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_GENERIC\"" -DLED_BUILTIN=2 -DFLASHMODE_DOUT  -DESP8266   -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\cores\esp8266" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\variants\generic" -I"C:\Users\nicol\Documents\Arduino\libraries\NewPing\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266WiFi\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ArduinoOTA" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266mDNS\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\Wire" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

lib\NewPingESP8266.cpp.o: ..\lib\NewPingESP8266.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:\Eclipse\arduinoPlugin\packages\esp8266\tools\xtensa-lx106-elf-gcc\3.0.4-gcc10.3-1757bed/bin/xtensa-lx106-elf-g++" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ -D_GNU_SOURCE "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/lwip2/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/libc/xtensa-lx106-elf/include" "-IC:\Eclipse\workspace\espcomedero\ESP01S/core" -c -w -Werror=return-type  -Os -g -free -fipa-pta -mlongcalls -mtext-section-literals -fno-rtti -falign-functions=4 -std=gnu++17 -MMD -ffunction-sections -fdata-sections -fno-exceptions  -DMMU_IRAM_SIZE=0x8000 -DMMU_ICACHE_SIZE=0x8000  -DNONOSDK22x_190703=1 -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=536 -DLWIP_FEATURES=1 -DLWIP_IPV6=0   -DARDUINO=10812 -DARDUINO_ESP8266_GENERIC -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_GENERIC\"" -DLED_BUILTIN=2 -DFLASHMODE_DOUT  -DESP8266   -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\cores\esp8266" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\variants\generic" -I"C:\Users\nicol\Documents\Arduino\libraries\NewPing\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266WiFi\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ArduinoOTA" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266mDNS\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\Wire" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

lib\Ota.cpp.o: ..\lib\Ota.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:\Eclipse\arduinoPlugin\packages\esp8266\tools\xtensa-lx106-elf-gcc\3.0.4-gcc10.3-1757bed/bin/xtensa-lx106-elf-g++" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ -D_GNU_SOURCE "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/lwip2/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/libc/xtensa-lx106-elf/include" "-IC:\Eclipse\workspace\espcomedero\ESP01S/core" -c -w -Werror=return-type  -Os -g -free -fipa-pta -mlongcalls -mtext-section-literals -fno-rtti -falign-functions=4 -std=gnu++17 -MMD -ffunction-sections -fdata-sections -fno-exceptions  -DMMU_IRAM_SIZE=0x8000 -DMMU_ICACHE_SIZE=0x8000  -DNONOSDK22x_190703=1 -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=536 -DLWIP_FEATURES=1 -DLWIP_IPV6=0   -DARDUINO=10812 -DARDUINO_ESP8266_GENERIC -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_GENERIC\"" -DLED_BUILTIN=2 -DFLASHMODE_DOUT  -DESP8266   -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\cores\esp8266" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\variants\generic" -I"C:\Users\nicol\Documents\Arduino\libraries\NewPing\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266WiFi\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ArduinoOTA" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266mDNS\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\Wire" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

lib\Paquete.cpp.o: ..\lib\Paquete.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:\Eclipse\arduinoPlugin\packages\esp8266\tools\xtensa-lx106-elf-gcc\3.0.4-gcc10.3-1757bed/bin/xtensa-lx106-elf-g++" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ -D_GNU_SOURCE "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/lwip2/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/libc/xtensa-lx106-elf/include" "-IC:\Eclipse\workspace\espcomedero\ESP01S/core" -c -w -Werror=return-type  -Os -g -free -fipa-pta -mlongcalls -mtext-section-literals -fno-rtti -falign-functions=4 -std=gnu++17 -MMD -ffunction-sections -fdata-sections -fno-exceptions  -DMMU_IRAM_SIZE=0x8000 -DMMU_ICACHE_SIZE=0x8000  -DNONOSDK22x_190703=1 -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=536 -DLWIP_FEATURES=1 -DLWIP_IPV6=0   -DARDUINO=10812 -DARDUINO_ESP8266_GENERIC -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_GENERIC\"" -DLED_BUILTIN=2 -DFLASHMODE_DOUT  -DESP8266   -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\cores\esp8266" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\variants\generic" -I"C:\Users\nicol\Documents\Arduino\libraries\NewPing\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266WiFi\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ArduinoOTA" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266mDNS\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\Wire" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

lib\Tcp.cpp.o: ..\lib\Tcp.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:\Eclipse\arduinoPlugin\packages\esp8266\tools\xtensa-lx106-elf-gcc\3.0.4-gcc10.3-1757bed/bin/xtensa-lx106-elf-g++" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ -D_GNU_SOURCE "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/lwip2/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/libc/xtensa-lx106-elf/include" "-IC:\Eclipse\workspace\espcomedero\ESP01S/core" -c -w -Werror=return-type  -Os -g -free -fipa-pta -mlongcalls -mtext-section-literals -fno-rtti -falign-functions=4 -std=gnu++17 -MMD -ffunction-sections -fdata-sections -fno-exceptions  -DMMU_IRAM_SIZE=0x8000 -DMMU_ICACHE_SIZE=0x8000  -DNONOSDK22x_190703=1 -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=536 -DLWIP_FEATURES=1 -DLWIP_IPV6=0   -DARDUINO=10812 -DARDUINO_ESP8266_GENERIC -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_GENERIC\"" -DLED_BUILTIN=2 -DFLASHMODE_DOUT  -DESP8266   -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\cores\esp8266" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\variants\generic" -I"C:\Users\nicol\Documents\Arduino\libraries\NewPing\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266WiFi\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ArduinoOTA" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266mDNS\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\Wire" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

lib\Utils.cpp.o: ..\lib\Utils.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:\Eclipse\arduinoPlugin\packages\esp8266\tools\xtensa-lx106-elf-gcc\3.0.4-gcc10.3-1757bed/bin/xtensa-lx106-elf-g++" -D__ets__ -DICACHE_FLASH -U__STRICT_ANSI__ -D_GNU_SOURCE "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/lwip2/include" "-IC:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2/tools/sdk/libc/xtensa-lx106-elf/include" "-IC:\Eclipse\workspace\espcomedero\ESP01S/core" -c -w -Werror=return-type  -Os -g -free -fipa-pta -mlongcalls -mtext-section-literals -fno-rtti -falign-functions=4 -std=gnu++17 -MMD -ffunction-sections -fdata-sections -fno-exceptions  -DMMU_IRAM_SIZE=0x8000 -DMMU_ICACHE_SIZE=0x8000  -DNONOSDK22x_190703=1 -DF_CPU=80000000L -DLWIP_OPEN_SRC -DTCP_MSS=536 -DLWIP_FEATURES=1 -DLWIP_IPV6=0   -DARDUINO=10812 -DARDUINO_ESP8266_GENERIC -DARDUINO_ARCH_ESP8266 "-DARDUINO_BOARD=\"ESP8266_GENERIC\"" -DLED_BUILTIN=2 -DFLASHMODE_DOUT  -DESP8266   -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\cores\esp8266" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\variants\generic" -I"C:\Users\nicol\Documents\Arduino\libraries\NewPing\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266WiFi\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ArduinoOTA" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\ESP8266mDNS\src" -I"C:\Eclipse\arduinoPlugin\packages\esp8266\hardware\esp8266\3.0.2\libraries\Wire" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '


